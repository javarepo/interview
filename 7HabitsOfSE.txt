Understand the scope and problem first before coding
Document as much as you can
Write readable & maintainable code
Be good at cross-functional work
Be an effective communicator
Recognize opportunities for improvements & impacts
Have a growth mind set