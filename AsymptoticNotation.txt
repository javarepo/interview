 1) Big Oh (O) ----- [ <= ] ==== [ Here 'O' stands for Order ]
 
 f(n) = O(g(n)) if and only if  f(n) <= C g(n) for some C > 0 after n >= n0 >= 0 
 
 Big Oh ( O ) is Least Upper bound 
 
 --- Worst case time / Upper bound of time 
 
 ------  O(1) represents a constant
 
 2) Big Omega ----- [ >= ]
 
 f(n) = Big Omega ( g(n)) if and only if f(n) >= C g(n) for some C > 0 after n >= n0 >= 0 
 
 Big Omega is Least lower bound
 
 --- Best case time / Lower bound of time
 
 3) Big Theta ----- [ = ]
 
 f(n) = theta ( g(n) ) if and only if C1 g(n) <= f(n) <= C2 g(n) for some C1 , C2  > 0 after n >= n0 >= 0

      C1 g(n) <= f(n) <= C2 g(n) for some C1 , C2  > 0 after n >= n0 >= 0       
 
    [ f(n) >= C1 g(n) ---- f(n) = Big omega ( g(n))  && f(n) <= C2 g(n) ---- f(n) = O(g(n)]
 
    ------ Average case 
	
	======================
	
	For an array of elements , Best case is Big omega(1) for finding an element in the array. 
    For an array of elements , Worst case is Big Oh(n) for finding an element in the array. 
    For an array of elements , Average case is Big theta(n/2) = Big theta(n) for finding an element in the array.	
	
  
 4) Small Oh (o) ----- [ < ]
 
    f(n) = o (g(n)))---- [ Here f(n) is asymptotically less than g(n) ]
 
 5) Small Omega (w) ----- [ > ]
 
 f(n) = small omega (g(n)))---- [ Here f(n) is asymptotically greater than g(n) ]
 
 
 
 
 =======================================
 
 Best Time Complexity : O(1) is better than O(n). Below is the order. 
 
 O(1) > O (log n) > O (n) > O (n log n)
 
 O(1) does not depend on input size. 
 
 
 